import React, {useState} from 'react';
import {Button} from 'react-native';
import PhotoGrid from './src/PhotoGrid';

export default function App() {
  const [filenames, setFilenames] = useState(['kitten00.jpg', 'kitten01.png']);

  function handleOnPress() {
    setFilenames(filenames.concat(['kitten02.jpeg']));
  }

  return (
    <>
      <Button title='MORE' onPress={handleOnPress} />
      <PhotoGrid filenames={filenames} />
    </>
  );
}
