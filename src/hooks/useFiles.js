import {useEffect, useRef, useReducer} from 'react';
import produce from 'immer';

import mockData from '../../data';
import {createAction, fsReadFile, fsWriteFile, fsExists} from '../utils';

const initialState = {};

const ADD = 'app/useFiles/add';
const UPDATE_FILE = 'app/useFiles/updateFile';

function reducer(state, action) {
  switch (action.type) {
    case ADD: {
      const nextState = produce(state, draft => {
        action.payload.forEach(filename => {
          if (typeof draft[filename] === 'undefined') {
            draft[filename] = {
              filename,
              exists: undefined,
              isLoading: false,
              data: undefined,
            };
          }
        });
      });
      return nextState;
    }
    case UPDATE_FILE: {
      const nextState = produce(state, draft => {
        const {filename, ...properties} = action.payload;
        draft[filename] = {...draft[filename], ...properties};
      });
      return nextState;
    }
    default:
      break;
  }
}

export default function useFiles(filenames) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const loadedFilesRef = useRef([]);

  const readFile = async filename => {
    try {
      dispatch(createAction(UPDATE_FILE, {filename, isLoading: true}));
      const data = await fsReadFile(filename, 'base64');
      dispatch(createAction(UPDATE_FILE, {filename, data, isLoading: false}));
      loadedFilesRef.current.push(filename);
    } catch (err) {
      throw new Error(err.message);
    }
  };

  useEffect(
    () => {
      const handleFile = async filename => {
        try {
          const exists = await fsExists(filename);

          dispatch(createAction(UPDATE_FILE, {filename, exists}));

          if (!exists) {
            try {
              dispatch(createAction(UPDATE_FILE, {filename, isLoading: true}));
              await fsWriteFile(filename, mockData[filename], 'base64');
              dispatch(createAction(UPDATE_FILE, {filename, exists: true}));
            } catch (err) {
              throw new Error(err.message);
            }
          }

          if (!loadedFilesRef.current.includes(filename)) {
            readFile(filename);
          }
        } catch (err) {
          throw new Error(err.message);
        }
      };

      dispatch(createAction(ADD, filenames));
      filenames.forEach(handleFile);
    },
    [filenames],
  );

  return Object.values(state);
}
