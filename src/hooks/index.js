import usePhotos from './usePhotos';
import useFiles from './useFiles';

export {usePhotos, useFiles};
