import {useState, useEffect, useRef} from 'react';
import RNFS from 'react-native-fs';

import mockData from '../../data';

export default function usePhotos(filenames) {
  const [photos, setPhotos] = useState([]);
  const photosRef = useRef([]);

  const writeFile = async filename => {
    await RNFS.writeFile(
      `${RNFS.DocumentDirectoryPath}/${filename}`,
      mockData[filename],
      'base64',
    );
  };

  useEffect(
    () => {
      const fileExists = async filename => {
        const exists = await RNFS.exists(
          `${RNFS.DocumentDirectoryPath}/${filename}`,
        );

        if (!exists) {
          await writeFile(filename);
        } else {
          const photo = {filename, data: mockData[filename]};
          if (!photosRef.current.find(ref => ref.filename === filename)) {
            setPhotos(photosRef.current.concat(photo));
            photosRef.current.push(photo);
          }
        }
      };
      filenames.forEach(filename => fileExists(filename));
    },
    [filenames],
  );

  return photos;
}
