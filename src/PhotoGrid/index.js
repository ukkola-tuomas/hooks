import React from 'react';
import {Image, ActivityIndicator} from 'react-native';
import {Row, Grid} from 'react-native-easy-grid';

import {useFiles, usePhotos} from '../hooks';

export default function PhotoGrid({filenames}) {
  const files = useFiles(filenames);

  return (
    <Grid>
      {files.map(file => {
        if (file.isLoading) {
          return (
            <Row
              key={file.filename}
              size={1}
              style={{justifyContent: 'center'}}
            >
              <ActivityIndicator size='large' />
            </Row>
          );
        }
        return (
          <Row key={file.filename} size={1}>
            <Image
              source={{uri: `data:image/*;base64,${file.data}`}}
              style={{width: '100%'}}
            />
          </Row>
        );
      })}
    </Grid>
  );
}
