import RNFS from 'react-native-fs';

export function createAction(type, payload) {
  return {type, payload};
}

export async function fsExists(filename) {
  return RNFS.exists(`${RNFS.DocumentDirectoryPath}/${filename}`);
}

export async function fsReadFile(filename, encoding) {
  return RNFS.readFile(`${RNFS.DocumentDirectoryPath}/${filename}`, encoding);
}

export async function fsWriteFile(filename, data, encoding) {
  return RNFS.writeFile(
    `${RNFS.DocumentDirectoryPath}/${filename}`,
    data,
    encoding,
  );
}
